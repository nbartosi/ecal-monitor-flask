echo "Activating the python environment..."
source ../env/bin/activate
echo "+ Done"

echo "Activating ROOT..."
source ../env/ext/root_build/bin/thisroot.sh
echo "+ Done"

echo "Setting proper locales..."
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

export FLASK_APP=$PWD/app/application.py
export FLASK_ENV=development
export FLASK_INSTANCE_PATH=$PWD/instance
echo "To run the app:"
echo "  flask run"
