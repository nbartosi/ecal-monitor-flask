echo "#### Starting the Flask WSGI application using Gunicorn"
echo "     available at address 127.0.0.1:8080"
gunicorn -b 127.0.0.1:8080 app:application
