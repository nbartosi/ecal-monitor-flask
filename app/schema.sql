DROP TABLE IF EXISTS distribution_type;
DROP TABLE IF EXISTS result;
DROP TABLE IF EXISTS tags;
DROP TABLE IF EXISTS result_tags;

CREATE TABLE distribution_type (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name varchar(255) UNIQUE NOT NULL,
    description TEXT
);

CREATE TABLE result (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    type_id INTEGER NOT NULL,
    data TEXT NOT NULL,
    comment TEXT,
    iov_start TIMESTAMP NOT NULL,
    iov_end TIMESTAMP NOT NULL,
    date_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (type_id) REFERENCES distribution_type (id)
);

CREATE TABLE tags (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name varchar(255) UNIQUE NOT NULL
);
CREATE UNIQUE INDEX idx_tags_name ON tags (name);

CREATE TABLE result_tags (
    result_id INTEGER NOT NULL,
    tag_id INTEGER NOT NULL,
    FOREIGN KEY (result_id) REFERENCES result (id),
    FOREIGN KEY (tag_id) REFERENCES tags (id),
    CONSTRAINT ids UNIQUE(result_id, tag_id)
);
CREATE INDEX idx_result_tags_result_id ON result_tags (result_id);
CREATE INDEX idx_result_tags_tag_id ON result_tags (tag_id);
