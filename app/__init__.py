import os
from flask import Flask, g
from flask_sqlalchemy import SQLAlchemy


def create_app(test_config=None):
    """Creating the basic configured application"""

    instance_path = os.environ.get('FLASK_INSTANCE_PATH')
    application = Flask(__name__, instance_path=instance_path, instance_relative_config=True)
    application.config.from_mapping(
        SECRET_KEY = 'dev',
        SQLALCHEMY_DATABASE_URI = 'sqlite:///'+os.path.join(application.instance_path, 'db.sqlite'),
        SQLALCHEMY_TRACK_MODIFICATIONS = False,
        UPLOAD_FOLDER = os.path.join(application.instance_path, 'uploads')
    )

    if test_config:
        # load the test config if passed in
        application.config.from_mapping(test_config)
    else:
        # load the instance config
        application.config.from_pyfile('config_flask.py', silent=False)

    # ensure the instance and upload folders exist
    for folder in [application.instance_path, application.config['UPLOAD_FOLDER']]:
        try:
            os.makedirs(folder)
        except OSError:
            pass

    # ensure the instance folder exists
    try:
        os.makedirs(application.instance_path)
    except OSError:
        pass

    return application


application = create_app()
db = SQLAlchemy(application)


from app.blueprints import main, upload, api
from app import classes

def register_interface(application):
    """Register all routes and commands"""
    # Registering Blueprints of available views
    application.register_blueprint(main.bp)
    application.register_blueprint(upload.bp)
    application.register_blueprint(api.bp)

    # Route for a test page
    @application.route('/test')
    def test():
        return 'This is a test page!\nIt works!'

    @application.route('/list/<int:order>')
    def show_list(order):
        return 'Showing the list for order: {0:d}'.format(order)

    # Registering commands
    # application.teardown_appcontext()
    application.cli.add_command(classes.init_db_command)
    application.cli.add_command(classes.fill_db_command)

register_interface(application)
