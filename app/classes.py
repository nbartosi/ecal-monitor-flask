import click
from flask import current_app, g
from flask.cli import with_appcontext
from app import db

HISTOGRAM_REQUIRED_KEYS = ['n', 'x', 'y', 'xIsTimestamp']

class Distribution(db.Model):
    """Type of distribution"""
    __tablename__ = 'distributions'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    description = db.Column(db.Text)

    def __repr__(self):
        return '<{0:s} | {1:s}>'.format(self.name, self.description)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'description': self.description
        }


# Tags association table for results
RESULT_TAGS = db.Table('result_tags', db.Model.metadata,
    db.Column('tag_id', db.ForeignKey('tags.id'), primary_key=True),
    db.Column('result_id', db.ForeignKey('results.id'), primary_key=True)
)


class Tag(db.Model):
    """Tags that can be referenced by a result"""
    __tablename__ = 'tags'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), index=True, nullable=False)
    results = db.relationship('Result', secondary=RESULT_TAGS, back_populates='tags')

    def __repr__(self):
        return '<{0:s}>'.format(self.name)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name
        }


class Result(db.Model):
    """Result entry"""
    __tablename__ = 'results'

    id = db.Column(db.Integer, primary_key=True)
    distribution = db.Column(db.ForeignKey('distributions.id'), index=True, nullable=False)
    data = db.Column(db.Text, nullable=False)
    comment = db.Column(db.Text)
    iov_start = db.Column(db.DateTime, index=True, nullable=False)
    iov_end = db.Column(db.DateTime, index=True, nullable=False)
    date_created = db.Column(db.DateTime, server_default=db.text('CURRENT_TIMESTAMP'), index=True, nullable=False)
    tags = db.relationship(Tag, secondary=RESULT_TAGS, back_populates='results')

    def to_dict(self):
        return {
            'id': self.id,
            'distribution': self.distribution,
            'data': self.data,
            'comment': self.comment,
            'date_created': self.date_created,
            'tags': self.tags
        }


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables"""
    db.create_all()
    click.echo('Initialized the database')


def fill_db():
    """Fills the DB with predefined information"""
    # Creating the distribution kinds
    distributions = {
        'Pedestal mean vs Run': 'Mean of pedestal values across all channels vs Run number',
        'Pedestal RMS vs Run': 'RMS of pedestal distribution across all channels vs Run number',
    }
    S = db.session
    S.add_all([Distribution(name=key, description=value) for key, value in distributions.items()])
    # Creating standard tags
    tags = ['EB', 'EE', 'EE+', 'EE-', 'thermalised']
    S.add_all([Tag(name=tag) for tag in tags])
    S.commit()


@click.command('fill-db')
@with_appcontext
def fill_db_command():
    """Insert predefined values to the DB"""
    fill_db()
    click.echo('Filled the database with predefined information')
