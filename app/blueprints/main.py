from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from flask import current_app as app

bp = Blueprint('main', __name__, url_prefix='/')


@bp.route('/', methods=['GET'])
def index():
    """Index page"""
    return render_template('index.html')

