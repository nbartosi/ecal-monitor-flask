from array import array
import json
import ROOT as R


def graph_from_json(json_str):
    """Creates a TGraph or TGraphAsymmErrors object from an input JSON string"""
    json_dict = json.loads(json_str)
    # Extracting common parameters in the creation of the objects
    n = json_dict['n']
    exl = array ('d',[0]*n )
    exh = array ('d',[0]*n )
    eyl = array ('d',[0]*n )
    eyh = array ('d',[0]*n )
    x = array( 'd' )
    y = array( 'd' )
    for i in range (n):
        x.append(json_dict['x'][i])
        y.append(json_dict['y'][i])
    if 'ytitle' in json_dict:
        json_ytitle = json_dict['ytitle']
    else :
        json_dict['ytitle'] = ''
    # Checking for errors at the X and Y axis
    if all (i in json_dict for i in ("exl","exh")):
        exl = array ('d',json_dict['exl'])
        exh = array ('d',json_dict['exh'])
    if all(i in json_dict for i in ("eyl","eyh")):
        eyl = array ('d',json_dict['eyl'])
        eyh = array ('d',json_dict['eyh'])
    # Creates a TGraphAsymmErrors if errors exist
    if any(sum(i) > 0 for i in (exl,exh,eyl,eyh)):
        gr1 = R.TGraphAsymmErrors(n,x,y,exl,exh,eyl,eyh)
    else:
        gr1 = R.TGraph(n,x,y)
    gr1.SetTitle( json_dict['title'] )
    gr1.GetYaxis().SetTitle( json_dict['ytitle'] )
    gr1.GetYaxis().SetRangeUser(min(y)-2,max(y)+2)
    return gr1

def root_from_json(json_list, out_path):
    """Stores ROOT objects created from JSON input into a ROOT file at out_path."""
    json_obj = []
    # Converting json list's objects using the graph_from_json() function
    for item in json_list:
        json_obj.append(graph_from_json(item))
    f = R.TFile(out_path,"RECREATE")
    for i in json_obj:
        i.Write('hist_{0:d}'.format(json_obj.index(i)))
    f.Close()
