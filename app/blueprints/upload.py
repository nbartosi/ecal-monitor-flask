import functools
import uuid
import os
import json
import shutil
import logging
import ROOT as R

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from flask import current_app as app
from werkzeug.utils import secure_filename
from app.data import upload_form_dict

ALLOWED_EXTENSIONS = set(['json', 'root'])
ALLOWED_CLASSES = set(['TH1C', 'TH1S', 'TH1I', 'TH1F', 'TH1D', 'TProfile', 'TGraph', 'TGraphErrors', 'TGraphAsymmErrors'])
# Timestamp of 01/01/2008 00:00:00
TIMESTAMP_MIN = 1199145600

HISTOGRAM_REQUIRED_KEYS = ['n', 'x', 'y', 'xIsTimestamp']

bp = Blueprint('upload', __name__, url_prefix='/upload')

def filename_valid(filename):
    """Check whether the file can be uploaded"""
    return '.' in filename and \
           os.path.splitext(filename)[1][1:].lower() in ALLOWED_EXTENSIONS


def extract_json(filepath):
    """Extract information from a JSON file"""
    f = open(filepath)
    json_str = f.read()
    result = {}
    try:
        python_dict = json.loads(json_str)
    except ValueError, e:
        logging.warning("Invalid JSON in file: {} ".format(filepath))
        return result
    # Detects if json input represents a dictionary or a nested dictionary
    if not any( type(value) == dict for value in python_dict.values()):
        temp_dict = {}
        temp_dict["undefined"] = python_dict
        python_dict = temp_dict
    for i, dic in python_dict.iteritems():
        # Ensuring the correct format of dictionary items
        if type(dic)!= dict or (any (j not in dic.keys() for j in HISTOGRAM_REQUIRED_KEYS)):
            logging.warning("Incomplete dictionary for key '{0}' in file: {1}" .format(i, filepath))
            continue
        else:
            i = i.encode('ascii')
            res = {}
            for j in ('n', 'y', 'x', 'title', 'xIsTimestamp', 'exl', 'exh', 'eyl', 'eyh', 'ytitle', 'xlabels'):
                if j in dic:
                    if type(dic[j]) == unicode:
                        res[j] = dic[j].encode ('ascii')
                    else:
                        res[j] = dic[j]
            result[i] = res
    return result


def extract_folder(folder,result,folders):
    """Extract information from folders inside a ROOT file"""
    for key in folder.GetListOfKeys():
        name = key.GetName()
        className = key.GetClassName()
        dic = {
            'class': className
        }
        obj = folder.Get(name)
        if key.IsFolder():
            folders_next = folders + [name]
            extract_folder(obj,result, folders_next)
            continue
        if className not in ALLOWED_CLASSES:
            continue
        # Initializing the dictionary for the histogram
        dic['n'] = 0
        for prop in ['x', 'y', 'exl', 'exh', 'eyl', 'eyh']:
            dic[prop] = []
        isHisto = obj.InheritsFrom('TH1')
        isGraph = obj.InheritsFrom('TGraph')
        dic['title'] = obj.GetTitle()
        dic['xIsTimestamp'] = True
        path = folders+[name]
        path = "{}".format("/".join(path))
        if isHisto:
            # Getting the list of points from the HISTOGRAM
            nPoints = obj.GetNbinsX()
            dic['n'] = nPoints
            dic['ytitle'] = obj.GetYaxis().GetTitle()
            for iP in range(nPoints):
                p = iP + 1
                x = obj.GetBinCenter(p)
                w = obj.GetBinWidth(p)
                dic['x'].append(x)
                dic['exl'].append(0.5*w)
                dic['exh'].append(0.5*w)
                dic['y'].append(obj.GetBinContent(p))
                dic['eyl'].append(obj.GetBinErrorLow(p))
                dic['eyh'].append(obj.GetBinErrorUp(p))
            # Checking for labels at the X axis
            labels = obj.GetXaxis().GetLabels()
            if bool(labels) is True:
                list_of_labels = []
                for iP in range(nPoints):
                    p = iP + 1
                    binlabel = obj.GetXaxis().GetBinLabel(p)
                    list_of_labels.append(binlabel)
                # Overwriting the x values if all lables are integer
                if all(i.isdigit() for i in list_of_labels):
                    dic['x'] = map(int,list_of_labels)
                    # Detect whether x axis represents run numbers or timestamps
                    if any(i< TIMESTAMP_MIN for i in dic['x']):
                        dic['xIsTimestamp'] = False
                else:
                    dic['xlabels'] = list_of_labels
            else :
                if any(i < TIMESTAMP_MIN for i in dic['x']):
                    dic['xIsTimestamp'] = False
        elif isGraph:
            # Getting the list of points from the GRAPH
            nPoints = obj.GetN()
            dic['n'] = nPoints
            for p in range(nPoints):
                x = R.Double(0)
                y = R.Double(0)
                obj.GetPoint(p, x, y)
                dic['x'].append(x)
                dic['y'].append(y)
                dic['exl'].append(obj.GetErrorXlow(p))
                dic['exh'].append(obj.GetErrorXhigh(p))
                dic['eyl'].append(obj.GetErrorYlow(p))
                dic['eyh'].append(obj.GetErrorYhigh(p))
            # Removing the X and Y errors if they do not exist
            if all(i<=0 for i in dic['exl']+dic['exh']):
                    del dic['exl']
                    del dic['exh']
            if all(i<=0 for i in dic['eyl']+dic['eyh']):
                    del dic['eyl']
                    del dic['eyh']
            if any(i < TIMESTAMP_MIN for i in dic['x']):
                    dic['xIsTimestamp'] = False
        result[path] = dic


def extract_root(filepath):
    """Extract information from a ROOT file"""
    file = R.TFile(filepath)
    result = {}
    folders = []
    extract_folder(file,result,folders)
    return result


def process_files(file_paths):
    """Extract content of the input files"""
    result = {}
    for file_path in file_paths:
        # Checking if input files exist before processing
        if not os.path.exists(file_path):
            logging.error("File does not exist: {} ".format(file_path))
            continue
        filename = os.path.basename(file_path)
        ext = os.path.splitext(file_path)[1][1:].lower()
        if ext == 'json':
            result[filename] = extract_json(file_path)
        elif ext == 'root':
            result[filename] = extract_root(file_path)
    return result


@bp.route('/', methods=['GET', 'POST'])
def index():
    """File upload form"""
    if request.method == 'GET':
        # Display the upload form
        return render_template('upload/index.html')
    else:
        if not 'file[]' in request.files:
            flash('No files submitted')
            return redirect(request.url)
        # Process the uploaded file
        files = request.files.getlist("file[]")
        # Creating a randomly generated folder for files from this request
        temp_folder = os.path.join(app.config['UPLOAD_FOLDER'], str(uuid.uuid4()))
        try:
            os.makedirs(temp_folder)
        except OSError:
            flash('Failed creating a temporary folder for files upload')
            return redirect(request.url)
        # Saving files to the temporary folder
        files_saved = []
        for file in files:
            # Skipping files without name
            if not file or file.filename == '':
                continue
            # Skipping files with wrong extension
            if not filename_valid(file.filename):
                continue
            # Saving file to the temporary directory
            filename = secure_filename(file.filename)
            file_path = os.path.join(temp_folder, filename)
            file.save(file_path)
            files_saved.append(file_path)
        # Processing the files
        contents = {}
        contents['histos'] = process_files(files_saved)
        # Adding information for the form population
        # contents['form_data'] = upload_form_dict()
        contents['form_data'] = {}
        # Removing the temporary folder
        shutil.rmtree(temp_folder)
        return json.dumps(contents, sort_keys=True, separators=(',',':'))
