import json

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, jsonify
)
from flask import current_app as app
from app import db
from app.classes import Distribution, Tag
from app.data import upload_form_dict

bp = Blueprint('api', __name__, url_prefix='/api')

@bp.route('/upload_info', methods=['GET'])
def upload_info():
    """Information useful for population of the Upload form"""
    data = upload_form_dict()
    return jsonify(data)

