## **Documentation of the histogram/graph representation**

##### An object extracted from a ROOT file is represented by a dictionary. The contents of the dictionary are as follows:

* **`class`** : `<string>` : *optional*
Class of the object.
`TH1` class if the dictionary is derived from a histogram or `TGraph` class if it is derived from a graph.

* **`n`** : `<int>`
Number of points.

* **`x`** : `<list[double]>`
X coordinates of the `n` points.
**Note** : Replaced by the labels of the X axis when all of them are digits and type `int`.

* **`y`** : `<list[double]>`
Y coordinates of the `n` points.

* **`exl`** : `<list[double]>` : *optional*
Lower error of a point on the X axis.
**Note** : Absent in a graph if all the points of the X axis have no or zero errors.

* **`exh`** : `<list[double]>` : *optional*
Upper error of a point on the X axis.
**Note** : Absent in a graph if all the points of the X axis have no or zero errors.

* **`eyl`** : `<list[double]>` : *optional*
Lower error of a point on the Y axis.
**Note** : Absent in a graph if all the points of the Y axis have no or zero errors.

* **`eyh`** : `<list[double]>` : *optional*
Upper error of a point on the Y axis.
**Note** : Absent in a graph if all the points of the Y axis have no or zero errors.

* **`title`** : `<string>` : *optional*
Title of the object.

* **`ytitle`** : `<string>` : *optional*
Title of the Y axis.

* **`xlabels`** : `<list[string]>` : *optional*
Labels of the X axis.
**Note** : Present only if automatic conversion to `int` failed.

* **`xIsTimestamp`** : `<bool>`
   * `True` - `x` values represent timestamps.
   * `False` - `x` values do not represent timestamps (presumably run numbers).
