from app.classes import Distribution, Tag

def upload_form_dict():
    """Get information useful for population of the Upload form"""
    data = {}
    distributions = Distribution.query.all()
    data['distributions'] = [d.to_dict() for d in Distribution.query.all()]
    data['tags'] = [t.to_dict() for t in Tag.query.all()]
    return data
