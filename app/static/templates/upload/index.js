(function($K, $V)
{
    // Creating the Vue application
    VUE_APP = new $V({
        el: '#vue-app',
        data: {
            histos: [],
            form_data: {}
        },
        methods: {
            // Fill in data from the upload response
            fillResponse: function(response) {
                var idx = 0;
                // Storing retrieved objects as elements of the module
                var histos = response['histos'];
                for (var file_name in histos) {
                    if (!histos.hasOwnProperty(file_name)) continue;
                    var file_obj = histos[file_name];
                    for (var histo_name in file_obj) {
                        if (!file_obj.hasOwnProperty(histo_name)) continue;
                        var histo = file_obj[histo_name];
                        histo_id = file_name+':'+histo_name;
                        histo['idx'] = idx;
                        histo['name'] = histo_name;
                        histo['file'] = file_name;
                        // Adding extra information
                        histo['iov_type'] = 'run'
                        histo['iov_start_run'] = undefined;
                        histo['iov_start_date'] = undefined;
                        histo['iov_start_time'] = '00:00';
                        histo['iov_end_run'] = undefined;
                        histo['iov_end_date'] = undefined;
                        histo['iov_end_time'] = '00:00';
                        histo['distribution'] = -1;
                        histo['tags'] = [];
                        // Storing histogram object to the histogram container
                        this.histos.push(histo);
                        idx++;
                    }
                }
                // Storing the form information
                form_data = response['form_data'];
                this.form_data['distributions'] = form_data['distributions']
                this.form_data['tags'] = [];
                for (var i in form_data['tags']) {
                    this.form_data['tags'].push({'text': form_data['tags'][i].name});
                }
            }
        }
    });

    // Single histogram form COMPONENT
    $V.component('histo-form', {
        props: ['histo', 'form_data'],
        data: function() {return {
            tag: ""
        }},
        computed: {
            date_start_id: function() {return 'iov_start_date_field_'+this.histo.idx;},
            date_start_id_hash: function() {return "#"+this.date_start_id},
            date_end_id: function() {return 'iov_end_date_field_'+this.histo.idx;},
            date_end_id_hash: function() {return "#"+this.date_end_id},
            autocomplete_tags: function() {
                var tags = [],
                    thisTag = this.tag.toLowerCase();
                for (var i in this.form_data.tags) {
                    var tag = this.form_data.tags[i];
                    if (this.histo.tags.indexOf(tag) !== -1) continue;
                    if (tag.text.toLowerCase().indexOf(thisTag) === -1) continue;
                    tags.push(tag);
                }
                return tags;
            }
        },
        template: `
        <div class="histo_form">
            <div class="is-highlight is-inset-8 is-inset-left-20"> 
                <div class="is-row">
                    <div class="is-col is-60 scrollable">
                        <var class="is-large">{{histo.file}}/{{histo.name}}</var> 
                    </div>
                    <div class="is-col is-40 is-text-right">
                        <small>{{histo.n}} points</small> &nbsp; &nbsp;
                        <small><a href="#" class="no-decor is-color-black" title="Copy inserted information to all other histograms">Copy to all</a></small> &nbsp; &nbsp;
                        <span class="close is-big" title="Don't upload this histogram"></span>
                    </div>
                </div>
            </div>
            <form class="is-inset-20">
                <div class="is-row">
                    <div class="form-item is-col">
                        <label>Distribution type <span class="is-req">*</span> <span class="is-desc">Contact developers if not in the list</span></label>
                        <!-- <input type="text" name="name"> -->
                        <select v-model="histo.distribution">
                            <option value="-1" disabled>Select the type</option>
                            <option v-for="option in form_data.distributions" v-bind:value="option.id" v-bind:title="option.description">{{option.id}}. {{option.name}}</option>
                        </select>
                    </div>
                    <div class="form-item is-col">
                        <label>Interval Of Validity <span class="is-req">*</span> <span class="is-desc">Date/time or Run # range</span></label>
                        <div class="is-row">
                            <div class="form-item is-bar is-col is-70" v-show="histo.iov_type == 'run'">
                                <input type="number" name="iov_start_run" class="is-30" placeholder="Start run"> <span>&nbsp; &#8594; &nbsp;</span> <input type="number" name="iov_end_run" class="is-30" placeholder="End run"> 
                            </div>
                            <div class="form-item is-bar is-col is-70" v-show="histo.iov_type == 'date'">
                                <div class="form-item is-row">
                                    <div class="form-item is-col is-70 is-append">
                                        <input type="text" name="iov_start_date" v-model="histo.iov_start_date" size="10" maxLength="10" placeholder="Start: yyyy/mm/dd">
                                        <a href="#" data-kube="datepicker" class="icon-kube-calendar" v-bind:data-name="'iov_start_date-'+histo.idx" data-target="#dump"></a>
                                    </div>
                                    <div class="form-item is-col is-30">
                                        <input type="time" name="iov_start_time" v-model="histo.iov_start_time" placeholder="HH:MM">
                                    </div>
                                </div>
                                <div class="form-item is-row">
                                    <div class="form-item is-col is-70 is-append">
                                        <input type="text" name="iov_end_date" v-model="histo.iov_end_date" size="10" maxLength="10" placeholder="End: yyyy/mm/dd">
                                        <a href="#" data-kube="datepicker" class="icon-kube-calendar" v-bind:data-name="'iov_end_date-'+histo.idx" data-target="#dump"></a>
                                    </div>
                                    <div class="form-item is-col is-30">
                                        <input type="time" name="iov_start_time" v-model="histo.iov_start_time" placeholder="HH:MM">
                                    </div>
                                </div>
                            </div>
                            <div class="form-item form-checkboxes is-col is-30">
                                <label class="is-checkbox"><input type="radio" value="run" name="iov_type" v-model="histo.iov_type"> <strong>Run#</strong></label> &nbsp;
                                <label class="is-checkbox"><input type="radio" value="date" name="iov_type" v-model="histo.iov_type"> &nbsp;<span class="icon-kube-calendar"></span></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-item">
                    <label>Tags <span class="is-req">*</span> <span class="is-desc">Comma separated list of words (tags can include spaces, not commas)</span></label>
                    <!-- <input type="text" name="tags" placeholder="To differentiate from other histograms of this type"> -->
                    <vue-tags-input v-model="tag" v-bind:tags="histo.tags" v-bind:autocomplete-items="autocomplete_tags" @tags-changed="newTags => histo.tags = newTags"></vue-tags-input>
                </div>
            </form>
        </div>
        `
    });


    // MODULE FOR UPLOAD RESPONSE ANALYSIS
    $K.add('module', 'upload-response', {
        ///////// ATTRIBUTES
        histograms: {},             // histogram container
        ///////// METHODS
        init: function(app, context) {
            this.app = VUE_APP;
        },
        // Building the list of histograms with response data
        _build: function(response) {
        },
        // Event responders
        onmessage: {
            // catch upload event
            upload: {
                start: function(sender) {
                    console.log('Upload started');
                },
                error: function(sender, response) {
                    console.err("Upload error");
                    console.log(response);
                },
                complete: function(sender, response) {
                    console.log("Upload finished!");
                    console.log(response);
                    this.app.fillResponse(response);
                    $K.dom('#upload_form').addClass('is-hidden');
                    $K.dom('#upload_result').removeClass('is-hidden');
                }
            },
            // catch date change in the datekeeper
            datepicker: {
                set: function(sender, date, obj) {
                    var parts = sender._id.split('-'),
                        field = parts[0],
                        idx = parseInt(parts[1]),
                        histo = VUE_APP.histos[idx][field] = [obj['year'], obj['month'], obj['day']].join('/');
                }
            }
        }
    });

})(Kube, Vue);
